package com.example.benchmarks;

import com.example.domain.Order;

import com.example.repositories.ArrayListRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class ArrayListBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        ArrayListRepository<Order> repository = new ArrayListRepository<>();
        Order order = new Order(1, 100, 1);
        Order order2 = new Order(2, 200, 2);
        Order order3 = new Order(3, 300, 3);
        Order order4 = new Order(4, 400, 4);
        Order order5 = new Order(5, 500, 5);
        public int size = 5;

        @Setup(Level.Iteration)
        public void setup() {
            repository.clear();
            repository.add(order);
            repository.add(order2);
            repository.add(order3);
            repository.add(order4);
            repository.add(order5);
        }
    }

    @Benchmark
    public void add(BenchmarkState state)
    {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.add(state.order);
        }
    }

    @Benchmark
    public void remove(BenchmarkState state) {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.remove(state.order);
        }
    }

    @Benchmark
    public void contains(BenchmarkState state) {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.contains(state.order);
        }
    }
}
