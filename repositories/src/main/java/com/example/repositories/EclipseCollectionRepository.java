package com.example.repositories;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;

public class EclipseCollectionRepository<T> implements InMemoryRepository<T> {
    private final MutableSet<T> set = Sets.mutable.empty();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) { set.remove(item); }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear(){
        set.clear();
    }
}

