package com.example.repositories;

import java.util.HashSet;

public class HashSetRepository<T> implements InMemoryRepository<T> {
    private final HashSet<T> set = new HashSet<>();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear(){
        set.clear();
    }
}