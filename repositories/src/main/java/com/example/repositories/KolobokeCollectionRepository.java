package com.example.repositories;

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;

public class KolobokeCollectionRepository<T> implements InMemoryRepository<T> {
    private final HashObjSet<T> set = HashObjSets.newMutableSet();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }
}
