import org.example.DoublePrimitiveOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class DoublePrimitiveTests {
    DoublePrimitiveOperations doublePrimitiveOperations;

    @BeforeEach
    void setUp() {
        doublePrimitiveOperations = new DoublePrimitiveOperations();
    }

    @Test
    void generateRandomDoubleList() {
        int size = 100;
        double[] generatedDoubles = doublePrimitiveOperations.getRandomList(size);
        Assertions.assertEquals(size, generatedDoubles.length);
    }

    @Test
    void computeSum() {
        double[] doubles = {10.5, 20.3, 5.2};
        double expectedSum = 36.0;
        double actualSum = doublePrimitiveOperations.getSum(doubles);
        Assertions.assertEquals(expectedSum, actualSum);
    }

    @Test
    void computeAverage() {
        double[] doubles = {10.5, 20.3, 5.2};
        double expectedAvg = 12.0;
        double actualAvg = doublePrimitiveOperations.getAverage(doubles);
        Assertions.assertEquals(expectedAvg, actualAvg);
    }

    @Test
    void top10() {
        double[] doubles = {12.5, 17.3, 8.9, 25.1, 19.7, 11.0, 15.8, 22.6, 14.3, 10.2, 16.5};
        double[] top10 = new double[0];
        try {
            top10 = doublePrimitiveOperations.getTop10(doubles);
        } catch (Exception ignored) {
        }

        double expected = 25.1;
        Assertions.assertTrue(Arrays.stream(top10).anyMatch(d -> d == expected));

        double[] doublesWrong = {12.5, 17.3, 8.9};
        var thrown = Assertions.assertThrows(Exception.class,
                () -> doublePrimitiveOperations.getTop10(doublesWrong));
        Assertions.assertEquals("List doest have 10 elements", thrown.getMessage());
    }
}
