package org.example.jmh;

import org.example.BigDecimalOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class BigDecimalRandomBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> randomOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            randomOrder = bigDecimalOperations.getRandomList(SIZE.size);
        }
    }
    @Benchmark
    public BigDecimal SumRandom(MyState state) {
        return state.bigDecimalOperations.getSum(state.randomOrder);
    }

    @Benchmark
    public BigDecimal AverageRandom(MyState state) {
        return state.bigDecimalOperations.getAverage(state.randomOrder);
    }

    @Benchmark
    public List<BigDecimal> top10PercentBiggestNumbersRandom(MyState state) throws Exception {
        return state.bigDecimalOperations.getTop10(state.randomOrder);
    }
}