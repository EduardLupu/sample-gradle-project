package org.example.jmh;

import org.example.DoublePrimitiveOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class DoublePrimitivesDescBenchmark {

    @State(Scope.Benchmark)
    public static class MyState {
        DoublePrimitiveOperations doublePrimitiveOperations = new DoublePrimitiveOperations();
        double[] descOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            descOrder = doublePrimitiveOperations.getRandomList(SIZE.size);
            Arrays.sort(descOrder);

            for (int i = 0; i < descOrder.length / 2; i++) {
                double aux = descOrder[i];
                descOrder[i] = descOrder[descOrder.length - i - 1];
                descOrder[descOrder.length - i - 1] = aux;
            }
        }
    }
    @Benchmark
    public Double SumDesc(MyState state) {
        return state.doublePrimitiveOperations.getSum(state.descOrder);
    }

    @Benchmark
    public Double AverageDesc(MyState state) {
        return state.doublePrimitiveOperations.getAverage(state.descOrder);
    }

    @Benchmark
    public double[] top10PercentBiggestNumbersDesc(MyState state) throws Exception {
        return state.doublePrimitiveOperations.getTop10(state.descOrder);
    }
}
