package org.example.jmh;

import org.example.DoublePrimitiveOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class DoublePrimitivesRandomBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        DoublePrimitiveOperations doublePrimitiveOperations = new DoublePrimitiveOperations();
        double[] randomOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            randomOrder = doublePrimitiveOperations.getRandomList(SIZE.size);
        }
    }

    @Benchmark
    public Double SumRandom(MyState state) {
        return state.doublePrimitiveOperations.getSum(state.randomOrder);
    }

    @Benchmark
    public Double AverageRandom(MyState state) {
        return state.doublePrimitiveOperations.getAverage(state.randomOrder);
    }

    @Benchmark
    public double[] top10PercentBiggestNumbersRandom(MyState state) throws Exception {
        return state.doublePrimitiveOperations.getTop10(state.randomOrder);
    }
}
