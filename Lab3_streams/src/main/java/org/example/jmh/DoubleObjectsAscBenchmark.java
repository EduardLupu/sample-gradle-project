package org.example.jmh;

import org.example.DoubleObjectOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class DoubleObjectsAscBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        DoubleObjectOperations doubleObjectOperations = new DoubleObjectOperations();
        List<Double> ascOrder;

        @Setup(Level.Trial)
        public void setUp() {
            ascOrder = doubleObjectOperations.getRandomList(SIZE.size);
            Collections.sort(ascOrder);
        }
    }

    @Benchmark
    public Double SumAsc(MyState state) {
        return state.doubleObjectOperations.getSum(state.ascOrder);
    }

    @Benchmark
    public Double AverageAsc(MyState state) {
        return state.doubleObjectOperations.getAverage(state.ascOrder);
    }

    @Benchmark
    public List<Double> top10PercentBiggestNumbersAsc(MyState state) throws Exception {
        return state.doubleObjectOperations.getTop10(state.ascOrder);
    }

}

