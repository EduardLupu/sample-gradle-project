package org.example.jmh;

import org.example.DoublePrimitiveOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class DoublePrimitivesAscBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        DoublePrimitiveOperations doublePrimitiveOperations = new DoublePrimitiveOperations();
        double[] ascOrder;

        @Setup(Level.Iteration)
        public void setUp() {
            ascOrder = doublePrimitiveOperations.getRandomList(SIZE.size);
            Arrays.sort(ascOrder);
        }
    }

    @Benchmark
    public Double SumAsc(MyState state) {
        return state.doublePrimitiveOperations.getSum(state.ascOrder);
    }

    @Benchmark
    public Double AverageAsc(MyState state) {
        return state.doublePrimitiveOperations.getAverage(state.ascOrder);
    }

    @Benchmark
    public double[] top10PercentBiggestNumbersAsc(MyState state) throws Exception {
        return state.doublePrimitiveOperations.getTop10(state.ascOrder);
    }
}
