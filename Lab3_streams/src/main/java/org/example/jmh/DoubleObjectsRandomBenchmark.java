package org.example.jmh;


import org.example.DoubleObjectOperations;
import org.example.SIZE;
import org.openjdk.jmh.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class DoubleObjectsRandomBenchmark {
    @State(Scope.Benchmark)
    public static class MyState {
        DoubleObjectOperations doubleObjectOperations = new DoubleObjectOperations();
        List<Double> randomOrder;

        @Setup(Level.Trial)
        public void setUp() {
            randomOrder = doubleObjectOperations.getRandomList(SIZE.size);

        }
    }

    @Benchmark
    public Double SumRandom(MyState state) {
        return state.doubleObjectOperations.getSum(state.randomOrder);
    }

    @Benchmark
    public Double AverageRandom(MyState state) {
        return state.doubleObjectOperations.getAverage(state.randomOrder);
    }

    @Benchmark
    public List<Double> top10PercentBiggestNumbersRandom(MyState state) throws Exception {
        return state.doubleObjectOperations.getTop10(state.randomOrder);
    }
}
