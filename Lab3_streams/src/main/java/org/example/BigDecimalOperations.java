package org.example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class BigDecimalOperations {

    public List<BigDecimal> getRandomList(int size) {
        List<BigDecimal> decimalList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            BigDecimal rand = BigDecimal.valueOf(random.nextDouble());
            decimalList.add(rand);
        }
        return decimalList;
    }

    public BigDecimal getSum(List<BigDecimal> decimalList) {
        return decimalList.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal getAverage(List<BigDecimal> decimalList) {
        BigDecimal sum = getSum(decimalList);
        return sum.divide(new BigDecimal(decimalList.size()), 2, RoundingMode.HALF_UP);
    }

    public List<BigDecimal> getTop10(List<BigDecimal> decimalList) throws Exception {
        int size = decimalList.size();

        if (size / 10 < 1) {
            throw new Exception("List doest have 10 elements");
        }

        return  decimalList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(size / 10)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

}
