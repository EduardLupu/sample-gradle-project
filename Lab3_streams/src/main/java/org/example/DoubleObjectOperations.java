package org.example;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class DoubleObjectOperations {
    public List<Double> getRandomList(int size) {
        List<Double> doublesList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            Double randomDouble = random.nextDouble();
            doublesList.add(randomDouble);
        }
        return doublesList;
    }

    public Double getSum(List<Double> doublesList) {
        return doublesList.stream().reduce(0.0, Double::sum);
    }

    public Double getAverage(List<Double> doublesList) {
        Double sum = getSum(doublesList);
        return sum / doublesList.size();
    }

    public List<Double> getTop10(List<Double> doublesList) throws Exception {
        int size = doublesList.size();

        if (size / 10 < 1) {
            throw new Exception("List doest have 10 elements");
        }

        return  doublesList.stream()
                .sorted(Comparator.reverseOrder())
                .limit(size / 10)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }
}
