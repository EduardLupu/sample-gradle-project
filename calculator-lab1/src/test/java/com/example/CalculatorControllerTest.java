package com.example;

import com.example.controller.CalculatorController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorControllerTest {
    private CalculatorController calculatorController;

    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        calculatorController = new CalculatorController();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void testWorkingOperation() {
        provideInput("5\n+\n3\n");
        calculatorController.run(1);
        assertTrue(outputStream.toString().contains("Result: 8.0"));
    }

    @Test
    public void testInvalidOperation() {
        provideInput("5\nunknown\n3\n");
        calculatorController.run(1);
        assertTrue(outputStream.toString().contains("Invalid operator!"));
    }

    private void provideInput(String input) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);
    }
}
