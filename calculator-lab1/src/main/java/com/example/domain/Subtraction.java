package com.example.domain;

public class Subtraction implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return a - b;
    }
}
