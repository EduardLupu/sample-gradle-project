package com.example.domain;

public class SquareRoot implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return Math.sqrt(a);
    }
}
