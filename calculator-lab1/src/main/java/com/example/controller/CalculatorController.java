package com.example.controller;

import com.example.service.CalculatorService;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class CalculatorController {
    private final CalculatorService calculatorService;

    public CalculatorController(){
        calculatorService = new CalculatorService();
    }

    public void run(int times) {
        System.out.println("Lab1 - Calculator");
        String[] operators = {"+", "-", "*", "/", "min", "max", "sqrt"};
        List<String> operatorList = Arrays.asList(operators);
        double a, b = 0, result;
        while (times >= 0) {
            System.out.println("===========================================");
            Scanner scanner = new Scanner(System.in);

            System.out.print("Operand 1: ");
            try {
                a = scanner.nextDouble();
            }
            catch (Exception e) {
                System.out.println("Invalid operand!");
                break;
            }

            System.out.print("Operation (" + String.join(", ", operators) + "): ");
            String operator = scanner.next();

            if (!operatorList.contains(operator)) {
                System.out.println("Invalid operator!");
                break;
            }

            if (!operator.equals("sqrt")) {
                System.out.print("Operand 2: ");
                try {
                    b = scanner.nextDouble();
                }
                catch (Exception e) {
                    System.out.println("Invalid operand!");
                    break;
                }
            }

            result = calculatorService.calculator(operator, a, b);
            System.out.println("\nResult: " + result);
            times--;
        }
    }
}
